<html>

 <!-- Fonts -->
 <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

<!-- Styles -->
<style>
  body{
    background: #eee;
    font-weight: 200;
    padding: 30px;
  text-align: center;
}
span{
    font-size:15px;
}
a{
  text-decoration:none; 
  color: #0062cc;
  border-bottom:2px solid #0062cc;
}
.box{
    padding:0px 0px;
}

.box-part{
    background:#FFF;
    border-radius:0;
    padding:20px 10px;
    margin:20px 0px;
}
.text{
    margin:20px 0px;
}

.fa{
     color:#4183D7;

}
.title {
                text-align: center;
                font-size: 40px;
            }
.subtitle {
    text-align: center;
    font-size: 25px;
    color:green;
    
            }

</style>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<body>
@guest
<a href = "{{ route('login')}}"  class="btn btn-light btn-sm float-right" style="font-weight: bold; float:right;margin-right: 20px;">LOGIN</a>
@endguest
@auth
<a href = "{{ route('logout')}}"  class="btn btn-light btn-sm float-right" style="font-weight: bold; float:right;margin-right: 20px;">LOGOUT</a>
@endauth

<br>
<br>

<div style="margin-left: 70px;" class="title">
      <h1 style="margin-left: 50px;">Corona Health Statements Supervision System</h1>
<br>
<p><a href =  "{{url('customers/create')}}" class="btn btn-secondary btn-lg">Customer Health Statement</a>
        <a href =  "{{url('business/create')}}" class="btn btn-secondary btn-lg">Application for business registration</a>

<div class="box">
    <div class="container">
     	<div class="row">
			 
			    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
               
					<div class="box-part text-center">
                        
                    <img src="https://i.ibb.co/fHGxMbL/2020-10-14-024856.png" alt="2020-10-14-024856" border="0"></a>
                        
						<div class="title">
							<h4>Wear a mask</h4>
						</div>
                        
						<div class="text">
							<span>Please keep wear a mask as long as you are not at your table.</span>
						</div>
                        
						                       
					 </div>
				</div>	 
				
				 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
               
					<div class="box-part text-center">
					    
					    <img src="https://i.ibb.co/9WRsh1d/2020-10-14-024158.png" alt="2020-10-14-024158" border="0"></a>
                    
						<div class="title">
							<h4>Measure heat</h4>
						</div>
                        
						<div class="text">
							<span>On the business entering, you have to measure heat.
 A person who has more than 38 degrees, can not enter!</span>
						</div>
                        
						                        
					 </div>
				</div>	 
				
				 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
               
					<div class="box-part text-center">
                        
                    <img src="https://i.ibb.co/wK15Rcp/2020-10-14-024133.png" alt="2020-10-14-024133" border="0"></a>
                        
						<div class="title">
							<h4>keep distance</h4>
						</div>
                        
						<div class="text">
							<span>Please be sure to keep a distance from other guests.</span>
						</div>
                        
						
					 </div>
				</div>	
				 
		
		</div>		
    </div>
</div>