

<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>



@php
 $user= Illuminate\Support\Facades\Auth::user();
 @endphp


   <!-- ####################################   Settings  ########################################   -->


    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>



<link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="../assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
Health Stataments Supervision   </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
   <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <!-- Styles -->
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" />
  <link href="{{ asset('assets/css/paper-dashboard.css')}}" rel="stylesheet" />
</head>






<div class="main-panel" style="  width: 100%; height: 100vh;">
    <!-- Navbar -->
        <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
            <div class="container-fluid">
            <div class="navbar-wrapper">
         
            @php
            @endphp
            @if (Auth::guest())


            <a class="navbar-brand" href="javascript:;"> Welcome Guest</a>
            @else

            <a class="navbar-brand" href="javascript:;"> Welcome Dear {{$user->name}}</a>
            @endif

          </div>
       
          
          <div class="collapse navbar-collapse justify-content-end" id="navigation">
         

            <ul class="navbar-nav">
              <li class="nav-item btn-rotate dropdown">
                <a class="nav-link dropdown-toggle" href="" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="nc-icon nc-settings-gear-65 "style="font-size: 1.3rem;"></i>
                  <p>
                    <span class="d-lg-none d-md-block">Some Actions</span>
                  </p>
                </a>
                
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                @guest

                  <a class="dropdown-item" href="{{ route('login') }}">{{ __('Login') }}</a>
                  @else
                  <a class="dropdown-item" href="{{ route('logout') }}">{{ __('Logout') }}</a>
                  @endguest

                </div>

              </li>
            </ul>
          </div>
        </div>
      </nav>


      
           <!-- End Navbar -->
           <div class="content" style=' max-width: 1500px; display: block; margin-left: auto; margin-right: auto;'>
        <div class="row">
          <div class="col-md-12">
            <div class="card">
           

          <main class="py-4" style="">






<script src="{{ asset('/js/app.js') }}" defer></script>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Create New Business') }}</div>

                <div class="card-body">
                    <form method = "post" action = "{{action('BusinessController@store')}}">
                  
                        @csrf

                        <div class="form-group row">
                            <label for="business_name" class="col-md-4 col-form-label text-md-right">{{ __('Business Name') }}</label>

                            <div class="col-md-6">
                                <input id="business_name" type="text" class="form-control @error('business_name') is-invalid @enderror" name="business_name" value="{{ old('business_name') }}" required autocomplete="business_name" autofocus>

                                @error('business_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="phone" class="col-md-4 col-form-label text-md-right">{{ __('Business Phone Number') }}</label>

                            <div class="col-md-6">
                                <input id="phone" type="number" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') }}" required autocomplete="phone">

                                @error('phone')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>






                        <div class="form-group row">
                            <label for="city_id" class="col-md-4 col-form-label text-md-right">Business City</label>
                            <div class="col-md-6">
                                <select required autocomplete="city_id" class="form-control" name="city_id">                                                                         
                                @foreach($cities as $city)
                                     <option value="{{ $city->id }}"> 
                                         {{ $city->Name }} 
                                     </option>
                                   @endforeach    
                                 </select>
                            </div>
                        </div>


        
         

                        <div class="form-group row">
                            <label for="address" class="col-md-4 col-form-label text-md-right">{{ __('Business Address') }}</label>

                            <div class="col-md-6">
                                <input id="address" type="text" class="form-control @error('address') is-invalid @enderror" name="address" value="{{ old('address') }}" required autocomplete="address" autofocus>

                                @error('address')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Business E-Mail') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

<br><br>
                        <div class="form-group row">
                            <label for="owner_name" class="col-md-4 col-form-label text-md-right">{{ __('Owner Name') }}</label>

                            <div class="col-md-6">
                                <input id="owner_name" type="text" class="form-control @error('owner_name') is-invalid @enderror" name="owner_name" value="{{ old('owner_name') }}" required autocomplete="owner_name" autofocus>

                                @error('owner_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label for="owner_email" class="col-md-4 col-form-label text-md-right">{{ __('Owner E-Mail') }}</label>

                            <div class="col-md-6">
                                <input id="owner_email" type="email" class="form-control @error('owner_email') is-invalid @enderror" name="owner_email" value="{{ old('owner_email') }}" required autocomplete="owner_email" autofocus>

                                @error('owner_email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                                <label for="owner_phone" class="col-md-4 col-form-label text-md-right">{{ __('Owner Phone Number') }}</label>
    
                                <div class="col-md-6">
                                    <input id="owner_phone" type="number" class="form-control @error('owner_phone') is-invalid @enderror" name="owner_phone" value="{{ old('owner_phone') }}" required autocomplete="owner_phone">
    
                                    @error('owner_phone')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
              
                            
                        
                           
                            <div class="form-group row">
                                <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>
    
                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
    
                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
    


                        <div>
                            <button type="submit" class="btn btn-primary btn-round">Create Business</button>
                           
                            </div>

                            </div>
                            @if (session('error'))
    <div class="alert alert-danger text-center msg" id="error">
    <strong>{{ session('error') }}</strong>
    </div>
@endif
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>




</main></div></div></div></div>
   


        
   <footer class="footer" style="position: absolute; bottom: 0; width: -webkit-fill-available;">
     <div class="container-fluid">
       <div class="row">
         <nav class="footer-nav">
           <ul>
             <li><a  target="_blank">Demo Release - CORONA Supervision System </a></li>
     
           </ul>
         </nav>
         <div class="credits ml-auto">
           <span class="copyright">
             © 2020, made with <i class="fa fa-heart heart"></i> by Evgeni, Adi, Shahar and Gal
           </span>
         </div>
       </div>
     </div>
   </footer>
 </div>
</div>
<!--   Core JS Files   -->
<script src="./assets/js/core/jquery.min.js"></script>
<script src="./assets/js/core/popper.min.js"></script>
<script src="./assets/js/core/bootstrap.min.js"></script>
<script src="./assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
<!--  Google Maps Plugin    -->
<script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
<!-- Chart JS -->
<script src="./assets/js/plugins/chartjs.min.js"></script>
<!--  Notifications Plugin    -->
<script src="./assets/js/plugins/bootstrap-notify.js"></script>
<!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
<script src="./assets/js/paper-dashboard.min.js?v=2.0.1" type="text/javascript"></script>


<script>
 function SelectText(element) {
   var doc = document,
     text = element,
     range, selection;
   if (doc.body.createTextRange) {
     range = document.body.createTextRange();
     range.moveToElementText(text);
     range.select();
   } else if (window.getSelection) {
     selection = window.getSelection();
     range = document.createRange();
     range.selectNodeContents(text);
     selection.removeAllRanges();
     selection.addRange(range);
   }
 }
 window.onload = function() {
   var iconsWrapper = document.getElementById('icons-wrapper'),
     listItems = iconsWrapper.getElementsByTagName('li');
   for (var i = 0; i < listItems.length; i++) {
     listItems[i].onclick = function fun(event) {
       var selectedTagName = event.target.tagName.toLowerCase();
       if (selectedTagName == 'p' || selectedTagName == 'em') {
         SelectText(event.target);
       } else if (selectedTagName == 'input') {
         event.target.setSelectionRange(0, event.target.value.length);
       }
     }

     var beforeContentChar = window.getComputedStyle(listItems[i].getElementsByTagName('i')[0], '::before').getPropertyValue('content').replace(/'/g, "").replace(/"/g, ""),
       beforeContent = beforeContentChar.charCodeAt(0).toString(16);
     var beforeContentElement = document.createElement("em");
     beforeContentElement.textContent = "\\" + beforeContent;
     listItems[i].appendChild(beforeContentElement);

     //create input element to copy/paste chart
     var charCharac = document.createElement('input');
     charCharac.setAttribute('type', 'text');
     charCharac.setAttribute('maxlength', '1');
     charCharac.setAttribute('readonly', 'true');
     charCharac.setAttribute('value', beforeContentChar);
     listItems[i].appendChild(charCharac);
   }
 }
</script>

</body>
</html>