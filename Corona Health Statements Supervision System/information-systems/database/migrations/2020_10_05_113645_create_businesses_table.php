<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBusinessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('businesses', function (Blueprint $table) {
            $table->id();
            $table->string('business_name', 20);	
            $table->string('phone', 20);	
            $table->bigInteger('city_id')->unsigned()->nullable();	
            $table->string('address', 40);	
            $table->string('email')->unique();
            $table->bigInteger('owner_id')->unsigned()->nullable();	
            $table->string('owner_email');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('businesses');
    }
}
