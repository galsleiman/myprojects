<?php

namespace App;
use Illuminate\Support\Facades\Auth;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','role_id','phone',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function role (){
        return $this->belongsTo('App\Role','role_id');
    } 
    public function isMinistryofHealth(){
        $uid = Auth::id();
        $user= User::findOrfail($uid);
        if($user->role_id===4) return true;
        return false;  
    }

    public function isManager(){
        $uid = Auth::id();
        $user= User::findOrfail($uid);
        if($user->role_id===1) return true;
        return false;  
    }
    public function isEmployee(){
        $uid = Auth::id();
        $user= User::findOrfail($uid);
        if($user->role_id===2) return true;
        return false;  
    }
    public static  function isOwner($uid){
        $user = Auth::user();
        $bid = $user->business_id; 
        $business = Business::findOrFail($bid);
        $oid= $business->owner_id;

        if($uid==$oid) return true;
        return false;  
    }

        public function business (){
        return $this->belongsTo('App\Business','id');
    }

    public function usersstatement (){
        return $this->hasMany('App\Usersstatement','user_id');
    }
}
