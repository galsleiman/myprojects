<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;


use App\Business;
use App\Customer;
use App\User;
use App\Usersstatement;
use App\Role;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['create','store']]);

    }
     public function index()
    {
        $user = Auth::user();
        $bid = $user->business_id; 
        $business = Business::findOrFail($bid);

        $users = User::where('business_id', $user->business_id)->paginate(8);
         #$users = User::paginate(2);
        return view('users.employee', compact('users','business'));    
    }

     
    
    public function usersStatmenteForm()
    {
        $user = Auth::user();
        #$userem = Usersstatement::all();
        return view('users.statement_form', compact('user'));    
    }
    
    public function usersStatmentes($id)
    {
        $user_statmentes = Usersstatement::where('user_id', $id)->orderBy('created_at','DESC')->paginate(10);


        #$userem = Usersstatement::all();
        return view('users.employee_statements', compact('user_statmentes'));    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $employee = new Usersstatement();

        if ($request->checkbox== TRUE){
            $employee = new Usersstatement();
            $employee -> user_id = Auth::id();
            $employee -> save();
             return redirect()->back()->with('success', 'statamate added successfully, you can add a new one  ');
            
      }else{
        return redirect()->back()->with('error', 'please confirm your health statamate');


      }


    }
    public function usersDashbors()
    {
        
        Gate::authorize('check-both');
        $users = Auth::user();
               
        return view('users.dashbord', compact('users'));    
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);

        $roles = Role::where('id', 1)->orWhere('id', 2)->get();
        
        return view('users.employee_edit' , compact('user','roles'));    }

        
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $user->name = $request->name;
        $user->phone = $request->phone;
        $user->email = $request->email;
        $user->password = Hash::make($request->password); 
        $user->role_id = $request->role_id;
        $user->save();


        return redirect('users');    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete(); 
        return redirect('users');    }

    public function deleteAll(Request $request)
    {
        $ids = $request->ids;
        DB::table("users")->whereIn('id',explode(",",$ids))->delete();
        return response()->json(['success'=>" The Employees Deleted Successfully "]);
    }

    public function back()
    {
        return redirect('users');
    }

    public function logout () {
        //logout user
        auth()->logout();
        // redirect to homepage
        return view('welcome');  
      
    }


}
