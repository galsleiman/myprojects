<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\DB;


use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     *
     * @var string
     */
    protected function authenticated(Request $request, $user)
    {
        if($user->role_id!=3){
            if($user->role_id==4){
                $users = Auth::user();
                $bus=DB::table('businesses')->where('role_id','=', 3)->count();
                return view('business.dashbordbus', compact('users','bus'));    
            }else{

            return view('users.dashbord');}
            


        } else {
            Auth::logout();


            return redirect('login')->with('msg', 'The Business Is Not Approved Yet, Try Again Later');; // it also be according to your need and routes
        }

    

    }
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    
}
